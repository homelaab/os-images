headless         = "false"

iso_url          = "./iso/ubuntu-20.04.3-live-server-amd64.iso"
#iso_url          = "http://releases.ubuntu.com/20.04.3/ubuntu-20.04.3-live-server-amd64.iso"
iso_checksum     = "sha256:f8e3086f3cea0fb3fefb29937ab5ed9d19e767079633960ccb50e76153effc98"

ssh_username     = "vagrant"
ssh_password     = "vagrant"

vm_name          = "ubuntu-focal"
output_directory = "./builds/ubuntu-20.04"
output_filename  = "ubuntu-20.04-server-amd64"

http_dir         = "./http"

boot_command     = [
    "<wait><enter><esc><wait5><f6><esc><wait>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs>",
    "<wait>",
    "/casper/vmlinuz ",
    "initrd=/casper/initrd quiet ",
    "<wait>",
    "autoinstall ",
    "<wait>",
    "ds=nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ --- ",
    "<enter><wait5>"
]