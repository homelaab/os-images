variable "headless" {
    type = bool
    default = false
}

variable "iso_url" {
    type = string
} 
variable "iso_checksum" {
    type = string
}

variable "ssh_username" {
    type = string
    default = "vagrant"
    sensitive = true
}

variable "ssh_password" {
    type = string
    default = "vagrant"
    sensitive = true
}

variable "vm_name" {
    type = string
    default = "ubuntu"
}
variable "output_directory" {
    type = string
    default = "./builds"
}
variable "output_filename" {
    type = string
}
variable "http_dir" {
    type = string
    default = "http"
}
variable "boot_command" {
    type = list(string)
}

source "virtualbox-iso" "ubuntu-autoinstall" {
    headless         = "${var.headless}"

    # ISO Specs
    iso_url         = "${var.iso_url}"
    iso_checksum     = "${var.iso_checksum}"

    # Default User
    ssh_username     = "${var.ssh_username}"
    ssh_password     = "${var.ssh_password}"
    ssh_timeout      = "15m"

    # Build Specs
    vm_name          = "${var.vm_name}"
    output_directory = "${var.output_directory}"
    output_filename  = "${var.output_filename}"

    # VM Specs
    vboxmanage       = [
        ["modifyvm", "{{.Name}}", "--memory", "1024"],
        ["modifyvm", "{{.Name}}", "--cpus", "1"],
    ]
    disk_size        = "5120"
    guest_os_type    = "Ubuntu_64"

    # Automatic installer config
    http_directory   = "${var.http_dir}"
    boot_command     = "${var.boot_command}"
    boot_wait        = "5s"

    # Shutdown
    shutdown_command = "echo 'vagrant' | sudo -S /usr/sbin/shutdown -P now"
}

build {
    sources = [
        "sources.virtualbox-iso.ubuntu-autoinstall"
    ]
    
    provisioner "ansible"  {
        playbook_file = "./ansible/playbook.yml"
    }

    post-processor "vagrant" {
        output = "vagrant/releases/${var.output_filename}_{{.Provider}}.box"
    }
    post-processor "shell-local" {
        inline = ["cd ./vagrant", "vagrant up"]
    }
}
