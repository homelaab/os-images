# os-images

Packer sample configuration that uses Ansible provisioner and Vagrant post-processor.

## Workspace preparation

You need to install these software on your system:

* [VirtualBox](https://www.virtualbox.org) - backend that runs virtual machines.
* [Packer](https://www.packer.io) - tool that creates OS images.
* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) - configuration management system that runs provisioning processes for OS images.
* [Vagrant](https://www.vagrantup.com) - tool that creates virtual environment from OS images.

You also need to install Vagrant plugin to specify VM disk size

```bash
vagrant plugin install vagrant-disksize
```

## Ubuntu Server 20.04

### Preparation

Download [Ubuntu Server 20.04 Live](https://releases.ubuntu.com/20.04/) (not legacy installer) into `iso` folder

### Build image

```bash
packer build -var-file=./vars/ubuntu-20.04-autoinstall.pkrvars.hcl template.pkr.hcl
```

After Packer build, Vagrant automatically runs VM 

### Use image

Connect to VM with command

```
vagrant ssh
```